package hotspotinternet.tlr600.main;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import org.springframework.web.client.RestTemplate;

import com.google.inject.AbstractModule;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import hotspotinternet.tlr600.driver.impl.Tlr600DriverModule;

/**
 * Guice module for binding Runner implementations for interfaces.
 */
public class RunnerModule extends AbstractModule {
    private final RestTemplate restTemplate;

    public RunnerModule(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    protected void configure() {
        bind(RestTemplate.class).toInstance(restTemplate);
        bind(Config.class).toInstance(ConfigFactory.load());
        bind(RestClient.class).to(RestClientImpl.class);
        bind(ScheduledExecutorService.class)
                .toInstance(Executors.newSingleThreadScheduledExecutor());
        install(new Tlr600DriverModule());
    }
}
