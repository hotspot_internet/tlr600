package hotspotinternet.tlr600.main;

import static com.google.common.base.Preconditions.checkNotNull;

import java.time.Instant;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * Class for storing information of a usage report. It describes an instant
 * at which the data usage is known.
 */
class InterfaceUsageReport {

    private int interfaceIndex;
    private Instant instant;
    private double usedPercent;

    /**
     * No-arg empty constructor to allow marshalling to/from JSON.
     */
    public InterfaceUsageReport() { }

    /**
     * Constructs InterfaceUsageReport from provided arguments.
     */
    @VisibleForTesting
    InterfaceUsageReport(
            final int interfaceIndex,
            final Instant instant,
            final double usedPercent) {
        this.interfaceIndex = checkNotNull(interfaceIndex);
        this.instant = checkNotNull(instant);
        this.usedPercent = checkNotNull(usedPercent);
    }

    public int getInterfaceIndex() {
        return interfaceIndex;
    }

    public double getUsedPercent() {
        return usedPercent;
    }

    public boolean isDataLeft() {
        return usedPercent < 100;
    }

    public Instant getInstant() {
        return instant;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("Interface Index", interfaceIndex)
                .add("Used Percent", usedPercent)
                .add("Instant", instant)
                .toString();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof InterfaceUsageReport)) {
            return false;
        }
        InterfaceUsageReport otherUsageReport = (InterfaceUsageReport) other;
        return interfaceIndex == otherUsageReport.interfaceIndex
                && usedPercent == otherUsageReport.usedPercent
                && Objects.equal(instant, otherUsageReport.instant);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(interfaceIndex, usedPercent, instant);
    }
}
