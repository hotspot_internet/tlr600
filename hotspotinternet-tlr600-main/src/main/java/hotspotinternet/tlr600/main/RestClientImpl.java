package hotspotinternet.tlr600.main;

import java.io.IOException;
import java.time.Duration;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import com.diffplug.common.base.Predicates;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.typesafe.config.Config;

import hotspotinternet.tlr600.driver.Tlr600Driver;
import hotspotinternet.tlr600.driver.impl.InterfaceReading;
import hotspotinternet.tlr600.driver.snmp.impl.AdminStatus;

/**
 * Implementation for handling interactions with the REST server.
 */
public class RestClientImpl implements RestClient {
    private static final Logger LOG = LoggerFactory.getLogger(RestClientImpl.class);
    private static final Duration POLL_INTERVAL = Duration.ofSeconds(60);
    private static final String REST_SERVER_KEY = "tlr600.rest.server.host";

    private final String restServerHost;
    private final ScheduledExecutorService executorService;
    private final RestTemplate restTemplate;
    private final Tlr600Driver tlr600Driver;

    private Future<?> scheduledFuture;

    /**
     * Construct from provided template.
     *
     * @param restTemplate for creating RestClient
     */
    @Inject
    RestClientImpl(
            final Config config,
            final Tlr600Driver tlr600Driver,
            final ScheduledExecutorService executorService,
            final RestTemplate restTemplate) {
        this.restServerHost = config.getString(REST_SERVER_KEY);
        this.tlr600Driver = tlr600Driver;
        this.executorService = executorService;
        this.restTemplate = restTemplate;
    }

    @Override
    public void run() throws IOException {
        tlr600Driver.open();
        scheduledFuture = executorService.scheduleWithFixedDelay(
                this::poll,
                0,
                POLL_INTERVAL.toMillis(),
                TimeUnit.MILLISECONDS);
    }

    private void poll() {
        List<InterfaceReading> readings = Collections.emptyList();
        try {
            readings = tlr600Driver.getInterfaceReadings();
        } catch (Throwable ex) {
            LOG.warn("Uncaught exception while getting readings: {}", ex);
        }
        postInterfaceReadings(readings);
        ensureEnabledStates(readings);
    }

    private void postInterfaceReadings(final List<InterfaceReading> readings) {
        for (InterfaceReading interfaceReading : readings) {
            try {
                postInterfaceReading(interfaceReading);
            } catch (Throwable ex) {
                LOG.warn("Uncaught exception while posting reading: {}", ex);
            }
        }
    }

    private InterfaceReading postInterfaceReading(final InterfaceReading interfaceReading) {
        final String url = String.format(
                "%s/reading/interface/%s",
                restServerHost,
                interfaceReading.getInterfaceIndex());
        LOG.debug("Posting interface reading [{}] to: {}", interfaceReading, url);
        InterfaceReading responseInterfaceReading = restTemplate.postForObject(
                url,
                interfaceReading,
                InterfaceReading.class);
        LOG.debug("Received response to post: {}", responseInterfaceReading);
        return responseInterfaceReading;
    }

    private void ensureEnabledStates(final List<InterfaceReading> readings) {
        final List<InterfaceReading> wanReadings = readings
                .stream()
                .filter(reading ->
                        tlr600Driver.getWanInterfaces()
                        .contains(reading.getInterfaceIndex()))
                .collect(Collectors.toList());
        try {
            final ImmutableMap<InterfaceReading, Boolean> readingToIsDataLeft = wanReadings
                    .stream()
                    .collect(ImmutableMap.toImmutableMap(
                            Function.identity(),
                            reading -> isDataLeft(reading.getInterfaceIndex())));
            boolean anyInterfaceHasDataLeft = readingToIsDataLeft.values().contains(true);
            for (InterfaceReading reading : wanReadings) {
                boolean desiredIsEnabled = anyInterfaceHasDataLeft
                        ? readingToIsDataLeft.get(reading)
                        : true;
                ensureEnabledState(reading, desiredIsEnabled);
            }
        } catch (Throwable ex) {
            LOG.warn("Uncaught exception while ensuring enabled states", ex);
        }
    }

    private void ensureEnabledState(
            final InterfaceReading reading,
            final boolean desiredIsEnabled) throws IOException {
        boolean isCorrectlyEnabled = desiredIsEnabled
                ? reading.getAdminStatus() == AdminStatus.UP
                : reading.getAdminStatus() == AdminStatus.DOWN;
        if (!isCorrectlyEnabled) {
            tlr600Driver.setInterfaceEnabled(reading.getInterfaceIndex(), desiredIsEnabled);
        }
    }

    private boolean isDataLeft(final int interfaceIndex) {
        final String url = String.format(
                "%s/notifications/usagereport/interface/%s",
                restServerHost,
                interfaceIndex);
        LOG.debug("Getting usage reports from REST URL: {}", url);
        InterfaceUsageReport[] usageReports = restTemplate.getForObject(
                url,
                InterfaceUsageReport[].class);
        List<InterfaceUsageReport> usageReportsList = usageReports == null
                ? Collections.emptyList()
                : Arrays.asList(usageReports);
        LOG.debug("Usage Reports: {}", usageReportsList);
        return !usageReportsList
                .stream()
                .filter(Predicates.not(InterfaceUsageReport::isDataLeft))
                .findAny()
                .isPresent();
    }

    @Override
    public void close() throws IOException {
        tlr600Driver.close();
        if (scheduledFuture != null) {
            scheduledFuture.cancel(true);
        }
    }
}
