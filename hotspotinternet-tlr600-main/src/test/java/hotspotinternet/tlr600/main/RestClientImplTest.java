package hotspotinternet.tlr600.main;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import java.io.IOException;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.InOrder;
import org.mockito.Mockito;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import com.typesafe.config.ConfigFactory;

import hotspotinternet.tlr600.driver.Tlr600Driver;
import hotspotinternet.tlr600.driver.impl.InterfaceReading;
import hotspotinternet.tlr600.driver.snmp.impl.AdminStatus;
import hotspotinternet.tlr600.driver.snmp.impl.OperStatus;

@SuppressWarnings({"rawtypes", "unchecked"})
public class RestClientImplTest {
    private static final long INT_MAX_LONG = (long) Integer.MAX_VALUE;
    private static final Instant INSTANT = Instant.now();
    private static final OperStatus OPER_STATUS = OperStatus.DOWN;
    private static final long IN_BYTES = 4 + INT_MAX_LONG;
    private static final long IN_DISCARDS = 5 + INT_MAX_LONG;
    private static final long IN_ERRORS = 6 + INT_MAX_LONG;
    private static final long IN_MULTICAST_PACKETS = 7 + INT_MAX_LONG;
    private static final long IN_UNICAST_PACKETS = 8 + INT_MAX_LONG;
    private static final long IN_UNKNOWN_PROTOCOLS = 9 + INT_MAX_LONG;
    private static final long OUT_BYTES = 10 + INT_MAX_LONG;
    private static final long OUT_DISCARDS = 11 + INT_MAX_LONG;
    private static final long OUT_ERRORS = 12 + INT_MAX_LONG;
    private static final long OUT_MULTICAST_PACKETS = 13 + INT_MAX_LONG;
    private static final long OUT_UNICAST_PACKETS = 14 + INT_MAX_LONG;

    private final ScheduledExecutorService mockExecutorService =
            mock(ScheduledExecutorService.class);
    private final Tlr600Driver mockDriver = mock(Tlr600Driver.class);

    private RestClient dut;
    private MockRestServiceServer mockServer;
    private ScheduledFuture future;

    private JSONObject createReadingJson(final int interfaceIndex,
            final AdminStatus adminStatus) {
        return new JSONObject()
                .put("measurementId", JSONObject.NULL)
                .put("interfaceIndex", interfaceIndex)
                // default parsing of Instant is a seconds (double) with nanosecond resolution
                .put("instant", INSTANT.getEpochSecond() + INSTANT.getNano() / 1.0e9)
                .put("adminStatus", adminStatus.toString())
                .put("operStatus", OPER_STATUS.toString())
                .put("inBytes", IN_BYTES)
                .put("inDiscards", IN_DISCARDS)
                .put("inErrors", IN_ERRORS)
                .put("inMulticastPackets", IN_MULTICAST_PACKETS)
                .put("inUnicastPackets", IN_UNICAST_PACKETS)
                .put("inUnknownProtocols", IN_UNKNOWN_PROTOCOLS)
                .put("outBytes", OUT_BYTES)
                .put("outDiscards", OUT_DISCARDS)
                .put("outErrors", OUT_ERRORS)
                .put("outMulticastPackets", OUT_MULTICAST_PACKETS)
                .put("outUnicastPackets", OUT_UNICAST_PACKETS);
    }

    private JSONObject createUsageReportJson(final int interfaceIndex, final double usedPercent) {
        return new JSONObject()
                .put("interfaceIndex", interfaceIndex)
                // default parsing of Instant is a seconds (double) with nanosecond resolution
                .put("instant", INSTANT.getEpochSecond() + INSTANT.getNano() / 1.0e9)
                .put("usedPercent", usedPercent);
    }

    private InterfaceReading createReading(
            final int interfaceIndex,
            final AdminStatus adminStatus) {
        return new InterfaceReading(null,
                interfaceIndex,
                INSTANT,
                adminStatus,
                OPER_STATUS,
                IN_BYTES,
                IN_DISCARDS,
                IN_ERRORS,
                IN_MULTICAST_PACKETS,
                IN_UNICAST_PACKETS,
                IN_UNKNOWN_PROTOCOLS,
                OUT_BYTES,
                OUT_DISCARDS,
                OUT_ERRORS,
                OUT_MULTICAST_PACKETS,
                OUT_UNICAST_PACKETS);
    }

    @BeforeEach
    public void setup() {
        RestTemplate restTemplate = new RestTemplate();
        mockServer = MockRestServiceServer.bindTo(restTemplate).build();
        future = mock(ScheduledFuture.class);
        when(mockDriver.getWanInterfaces()).thenReturn(Arrays.asList(1, 2, 3));
        when(mockExecutorService.scheduleWithFixedDelay(
                argThat(new ArgumentMatcher<Runnable>() {
                    @Override
                    public boolean matches(Runnable argument) {
                        argument.run();
                        return true;
                    }
                }),
                anyLong(),
                anyLong(),
                any())).thenReturn(future);
        dut = new RestClientImpl(
                ConfigFactory.load(),
                mockDriver,
                mockExecutorService,
                restTemplate);
    }

    @Test
    public void testClose() throws Exception {
        // when
        dut.close();
        dut.run();
        dut.close();

        // then
        final InOrder inOrder = Mockito.inOrder(mockDriver, future);
        inOrder.verify(mockDriver).open();
        inOrder.verify(mockDriver).close();
        inOrder.verify(future).cancel(true);
    }

    @Test
    public void testDriverOpenException() throws Exception {
        doThrow(IOException.class).when(mockDriver).open();
        Assertions.assertThrows(IOException.class, dut::run);
    }

    @Test
    public void testGetReadingsException() throws Exception {
        when(mockDriver.getInterfaceReadings())
                .thenAnswer(invocation -> {
                    throw new Throwable();
                })
                .thenReturn(Collections.EMPTY_LIST);
        dut.run();
        dut.run();
        verify(mockDriver, times(2)).getInterfaceReadings();
    }

    private static Matcher<String> jsonMatcher(final JSONObject expectedJsonObject) {
        return new BaseMatcher<String>() {
            @Override
            public boolean matches(Object actual) {
                if (!(actual instanceof String)) {
                    return false;
                }
                JSONObject actualJsonObject = new JSONObject((String) actual);
                return expectedJsonObject.similar(actualJsonObject);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(expectedJsonObject.toString());
            }
        };
    }

    @Test
    public void testAllInterfacesOutOfData() throws Exception {
        // given
        final JSONObject responseReadingJson1 = createReadingJson(1, AdminStatus.DOWN);
        final JSONObject responseReadingJson2 = createReadingJson(2, AdminStatus.UP);
        final JSONArray responseUsageReport1 = new JSONArray(Arrays.asList(
                createUsageReportJson(1, 100.0)));
        final JSONArray responseUsageReport2 = new JSONArray(Arrays.asList(
                createUsageReportJson(2, 100.0)));
        when(mockDriver.getInterfaceReadings()).thenReturn(Arrays.asList(
                createReading(1, AdminStatus.DOWN),
                createReading(2, AdminStatus.UP)));

        // when
        mockReadingResponse(1, responseReadingJson1);
        mockReadingResponse(2, responseReadingJson2);
        mockUsageReportResponse(1, responseUsageReport1);
        mockUsageReportResponse(2, responseUsageReport2);
        dut.run();

        // then
        verify(mockDriver).open();
        verify(mockDriver, atLeast(1)).getWanInterfaces();
        verify(mockDriver).getInterfaceReadings();
        verify(mockDriver).setInterfaceEnabled(1, true);
        verifyNoMoreInteractions(mockDriver);
        mockServer.verify();
    }

    @Test
    public void testOneInterfaceOutOfData() throws Exception {
        // given
        final JSONObject responseReadingJson1 = createReadingJson(1, AdminStatus.UP);
        final JSONObject responseReadingJson2 = createReadingJson(2, AdminStatus.UP);
        final JSONArray responseUsageReport1 = new JSONArray(Arrays.asList(
                createUsageReportJson(1, 90.0),
                createUsageReportJson(1, 100.0),
                createUsageReportJson(1, 90.0)));
        final JSONArray responseUsageReport2 = new JSONArray(Arrays.asList(
                createUsageReportJson(2, 90.0)));
        when(mockDriver.getInterfaceReadings()).thenReturn(Arrays.asList(
                createReading(1, AdminStatus.UP),
                createReading(2, AdminStatus.UP)));

        // when
        mockReadingResponse(1, responseReadingJson1);
        mockReadingResponse(2, responseReadingJson2);
        mockUsageReportResponse(1, responseUsageReport1);
        mockUsageReportResponse(2, responseUsageReport2);
        dut.run();

        // then
        verify(mockDriver).open();
        verify(mockDriver, atLeast(1)).getWanInterfaces();
        verify(mockDriver).getInterfaceReadings();
        verify(mockDriver).setInterfaceEnabled(1, false);
        verifyNoMoreInteractions(mockDriver);
        mockServer.verify();
    }

    private void mockReadingResponse(final int interfaceIndex, final JSONObject readingJson) {
        mockServer
                .expect(requestTo(String.format(
                        "https://192.168.151.171:8081/reading/interface/%s",
                        interfaceIndex)))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().string(jsonMatcher(readingJson)))
                .andRespond(withSuccess(
                        readingJson.toString(),
                        MediaType.APPLICATION_JSON));
    }

    private void mockUsageReportResponse(
            final int interfaceIndex,
            final JSONArray usageReportsJson) {
        mockServer
                .expect(requestTo(String.format(
                        "https://192.168.151.171:8081/notifications/usagereport/interface/%s",
                        interfaceIndex)))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(
                        usageReportsJson.toString(),
                        MediaType.APPLICATION_JSON));
    }
}
