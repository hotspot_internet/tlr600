# tlr600

Runner and driver for communicating with TP-Link multi-wan router (Model TL-R600VPN).

## Details

The driver polls the switch hardware for various status information via SNMP. The
driver may also configure some settings on the switch via SNMP. A REST API is
used to publish the queried data to the backend database. It is expected that this
software will run on the same LAN network where the router hardware resides. See
design documents in artifacts of this project for additional details.

## Configuration

Using the executable within the Spring boot application (created from
`./gradlew build` command), the application can be run in the following manner:

    ./<app-executable> --<spring-property-name>=<property-value>

The following outlines spring properties that may be of particular interest
to override/set:

* General (application.conf):
    * tlr600.rest.server.host = "<REST server IP>"
* Spring Security (application.properties OR command-line args):
    * http.client.ssl.key-store = /path/to/keystore.p12
    * http.client.ssl.key-store-password = <keystore password>
    * http.client.ssl.trust-store = /path/to/truststore.jks
    * http.client.ssl.trust-store-password = <truststore password>

NOTE: The Spring security settings do not have defaults and therefore must be
configured in order for the application to run.
