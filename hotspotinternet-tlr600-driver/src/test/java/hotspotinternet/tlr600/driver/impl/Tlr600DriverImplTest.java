package hotspotinternet.tlr600.driver.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map.Entry;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.Integer32;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.VariableBinding;

import com.google.common.collect.ImmutableMap;
import com.typesafe.config.ConfigFactory;

import hotspotinternet.tlr600.driver.Tlr600Driver;
import hotspotinternet.tlr600.driver.snmp.Tlr600Snmp;
import hotspotinternet.tlr600.driver.snmp.impl.AdminStatus;
import hotspotinternet.tlr600.driver.snmp.impl.IfExTableColumn;
import hotspotinternet.tlr600.driver.snmp.impl.InterfacesColumn;
import hotspotinternet.tlr600.driver.snmp.impl.OperStatus;


public class Tlr600DriverImplTest {
    private static final Instant TEST_INSTANT = Instant.now();
    // SNMP interface 2 and 5 are for the same MAC address, so results for those interfaces
    // will be aggregated (in case of Counter fields, this means summing; for OperStatus
    // and AdminStatus, this means taking the value for the highest SNMP index).
    // SNMP interface 3 is the only SNMP entry for that MAC address.
    // SNMP interface 5 corresponds to an unexpected MAC address *:f9 and therefore entries
    // for that SNMP iface index will not show up in any of the InterfaceReadings
    private static final ImmutableMap<Integer, String> IFACE_ADDRESSES =
            ImmutableMap.of(
                    2, "d8:47:32:d8:f6:f5",
                    3, "d8:47:32:d8:f6:f6",
                    4, "d8:47:32:d8:f6:f9",
                    5, "d8:47:32:d8:f6:f5");
    // List<Integer> indicates the values that will be returned for SNMP iface indices
    // 2, 3, 4, 5 (in that order). Corresponds to the entries of IFACE_ADDRESSES above
    private static final ImmutableMap<Supplier<OID>, List<Integer>> FIELD_VALUES =
            new ImmutableMap.Builder<Supplier<OID>, List<Integer>>()
                    .put(InterfacesColumn.IF_ADMIN_STATUS, Arrays.asList(1, 2, 1, 3))
                    .put(InterfacesColumn.IF_OPER_STATUS, Arrays.asList(1, 2, 3, 4))
                    .put(IfExTableColumn.IF_HC_IN_OCTETS, Arrays.asList(10, 20, 30, 40))
                    .put(InterfacesColumn.IF_IN_DISCARDS, Arrays.asList(11, 21, 31, 41))
                    .put(InterfacesColumn.IF_IN_ERRORS, Arrays.asList(12, 22, 32, 42))
                    .put(IfExTableColumn.IF_HC_IN_MULTICAST_PKTS, Arrays.asList(13, 23, 33, 43))
                    .put(IfExTableColumn.IF_HC_IN_UCAST_PKTS, Arrays.asList(14, 24, 34, 44))
                    .put(InterfacesColumn.IF_IN_UNKNOWN_PROTOS, Arrays.asList(15, 25, 35, 45))
                    .put(IfExTableColumn.IF_HC_OUT_OCTETS, Arrays.asList(20, 30, 40, 50))
                    .put(InterfacesColumn.IF_OUT_DISCARDS, Arrays.asList(21, 31, 41, 51))
                    .put(InterfacesColumn.IF_OUT_ERRORS, Arrays.asList(22, 32, 42, 52))
                    .put(IfExTableColumn.IF_HC_OUT_MULTICAST_PKTS, Arrays.asList(23, 33, 43, 53))
                    .put(IfExTableColumn.IF_HC_OUT_UCAST_PKTS, Arrays.asList(24, 34, 44, 54))
                    .build();

    private Tlr600Driver dut;
    private Tlr600Snmp mockSnmp;

    @Before
    public void setupDut() throws Exception {
        mockSnmp = mock(Tlr600Snmp.class);
        when(mockSnmp.get(SnmpConstants.sysName)).thenReturn(
                new VariableBinding(
                        SnmpConstants.sysName,
                        new OctetString("name")));
        dut = new Tlr600DriverImpl(ConfigFactory.load(), () -> TEST_INSTANT, mockSnmp);
    }

    @Test
    public void testOpen() throws Exception {
        dut.open();
        verify(mockSnmp).open();
    }

    @Test(expected = IOException.class)
    public void testOpenException() throws Exception {
        doThrow(IOException.class).when(mockSnmp).open();
        dut.open();
    }

    // just a smoke test that things don't blow up
    @Test
    public void testClose() throws Exception {
        dut.close();
        dut.open();
        dut.close();
    }

    @Test
    public void testGetInterfaceReadings() throws Exception {
        // given
        mockSnmpWalks();

        // when
        dut.open();
        List<InterfaceReading> interfaceReadings = dut.getInterfaceReadings();

        // then
        // see comments for static finals at the top of this class for details
        assertEquals(2, interfaceReadings.size());
        assertTrue(interfaceReadings.contains(
                new InterfaceReading(
                        null, 1, TEST_INSTANT, AdminStatus.UP, OperStatus.UP,
                        50L, 52L, 54L, 56L, 58L, 60L,
                        70L, 72L, 74L, 76L, 78L)));
        assertTrue(interfaceReadings.contains(
                new InterfaceReading(
                        null, 2, TEST_INSTANT, AdminStatus.DOWN, OperStatus.DOWN,
                        20L, 21L, 22L, 23L, 24L, 25L,
                        30L, 31L, 32L, 33L, 34L)));
    }

    @Test
    public void testSetInterfaceEnabled() throws Exception {
        // given
        mockSnmpWalks();

        // when
        dut.open();
        dut.setInterfaceEnabled(1, true);

        // then
        verify(mockSnmp).set(InterfacesColumn.IF_ADMIN_STATUS.get(2), new Integer32(1));
    }

    @Test
    public void testSetInterfaceDisabled() throws Exception {
        // given
        mockSnmpWalks();

        // when
        dut.open();
        dut.setInterfaceEnabled(1, false);

        // then
        verify(mockSnmp).set(InterfacesColumn.IF_ADMIN_STATUS.get(2), new Integer32(2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEnableInvalidInterface() throws Exception {
        // given
        mockSnmpWalks();

        // when
        dut.open();
        dut.setInterfaceEnabled(5, true);
    }

    private void mockSnmpWalks() {
        when(mockSnmp.walk(InterfacesColumn.IF_PHYS_ADDRESS.get())).thenReturn(
                IFACE_ADDRESSES.entrySet().stream()
                .map(entry ->
                        createInterfaceBinding(
                                InterfacesColumn.IF_PHYS_ADDRESS, entry.getKey(), entry.getValue()))
                .collect(Collectors.toList()));

        final List<Integer> snmpIfacIndices = IFACE_ADDRESSES.keySet().asList();
        for (Entry<Supplier<OID>, List<Integer>> entry : FIELD_VALUES.entrySet()) {
            final OID oid = entry.getKey().get();
            final List<Integer> values = entry.getValue();

            final List<VariableBinding> variableBindings = new ArrayList<>();
            for (int ifaceListIndex = 0;
                    ifaceListIndex < snmpIfacIndices.size();
                    ++ifaceListIndex) {
                variableBindings.add(
                        createInterfaceBinding(
                                () -> oid,
                                snmpIfacIndices.get(ifaceListIndex),
                                values.get(ifaceListIndex)));
            }
            when(mockSnmp.walk(oid)).thenReturn(variableBindings);
        }
    }

    private VariableBinding createInterfaceBinding(
            final Supplier<OID> oidSupplier,
            final int snmpIfIndex,
            final String variableString) {
        return new VariableBinding(
                new OID(oidSupplier.get().getValue(), snmpIfIndex),
                new OctetString(variableString));
    }

    private VariableBinding createInterfaceBinding(
            final Supplier<OID> oidSupplier,
            final int snmpIfIndex,
            final int variableInt) {
        return new VariableBinding(
                new OID(oidSupplier.get().getValue(), snmpIfIndex),
                new Integer32(variableInt));
    }
}
