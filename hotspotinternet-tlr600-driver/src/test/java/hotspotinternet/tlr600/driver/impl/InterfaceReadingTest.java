package hotspotinternet.tlr600.driver.impl;

import static org.junit.Assert.assertNull;

import org.junit.Test;


public class InterfaceReadingTest {

    // Users of this class may not set certain fields (e.g. "outBytes"). Using
    // a primitive type (i.e. int) would result in default value of 0, while null
    // is a more appropriate default value. This test checks that Nullable fields
    // are null.
    @Test
    public void testNullFieldsDefaultConstructor() throws Exception {
        InterfaceReading interfaceReading = new InterfaceReading();
        assertNull(interfaceReading.getMeasurementId());
        assertNull(interfaceReading.getInBytes());
        assertNull(interfaceReading.getInDiscards());
        assertNull(interfaceReading.getInErrors());
        assertNull(interfaceReading.getInMulticastPackets());
        assertNull(interfaceReading.getInUnicastPackets());
        assertNull(interfaceReading.getInUnknownProtocols());
        assertNull(interfaceReading.getOutBytes());
        assertNull(interfaceReading.getOutDiscards());
        assertNull(interfaceReading.getOutErrors());
        assertNull(interfaceReading.getOutMulticastPackets());
        assertNull(interfaceReading.getOutUnicastPackets());
    }
}
