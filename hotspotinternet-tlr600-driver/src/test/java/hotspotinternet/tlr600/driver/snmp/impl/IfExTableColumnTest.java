package hotspotinternet.tlr600.driver.snmp.impl;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.snmp4j.smi.OID;


public class IfExTableColumnTest {

    @Test
    public void testIfNameOid() throws Exception {
        assertEquals(
                new OID(new int[] {1, 3, 6, 1, 2, 1, 31, 1, 1, 1, 1}),
                IfExTableColumn.IF_NAME.get());
    }

    @Test
    public void testIfHcOutOctetsOid() throws Exception {
        assertEquals(
                new OID(new int[] {1, 3, 6, 1, 2, 1, 31, 1, 1, 1, 10}),
                IfExTableColumn.IF_HC_OUT_OCTETS.get());
    }
}
