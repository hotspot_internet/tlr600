package hotspotinternet.tlr600.driver.snmp.impl;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.snmp4j.smi.OID;


public class InterfacesColumnTest {

    @Test
    public void testIfAdminStatus() throws Exception {
        assertEquals(
                new OID(new int[] {1, 3, 6, 1, 2, 1, 2, 2, 1, 7}),
                InterfacesColumn.IF_ADMIN_STATUS.get());
    }

    @Test
    public void testIfIndex() throws Exception {
        assertEquals(
                new OID(new int[] {1, 3, 6, 1, 2, 1, 2, 2, 1, 1}),
                InterfacesColumn.IF_INDEX.get());
    }
}
