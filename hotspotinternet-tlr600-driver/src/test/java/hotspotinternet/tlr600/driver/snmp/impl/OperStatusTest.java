package hotspotinternet.tlr600.driver.snmp.impl;

import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class OperStatusTest {

    @Test
    public void testOperStatusUp() throws Exception {
        assertEquals(
                OperStatus.UP,
                OperStatus.fromSnmpValue(1));
    }

    @Test
    public void testOperStatusDown() throws Exception {
        assertEquals(
                OperStatus.DOWN,
                OperStatus.fromSnmpValue(2));
    }

    @Test
    public void testOperStatusTesting() throws Exception {
        assertEquals(
                OperStatus.TESTING,
                OperStatus.fromSnmpValue(3));
    }

    @Test
    public void testOperStatusUnknown() throws Exception {
        assertEquals(
                OperStatus.UNKNOWN,
                OperStatus.fromSnmpValue(4));
    }

    @Test
    public void testOperStatusDormant() throws Exception {
        assertEquals(
                OperStatus.DORMANT,
                OperStatus.fromSnmpValue(5));
    }

    @Test
    public void testOperStatusNotPresent() throws Exception {
        assertEquals(
                OperStatus.NOT_PRESENT,
                OperStatus.fromSnmpValue(6));
    }

    @Test
    public void testOperStatusLowerDown() throws Exception {
        assertEquals(
                OperStatus.LOWER_LAYER_DOWN,
                OperStatus.fromSnmpValue(7));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOperStatusInvalid() throws Exception {
        OperStatus.fromSnmpValue(8);
    }
}
