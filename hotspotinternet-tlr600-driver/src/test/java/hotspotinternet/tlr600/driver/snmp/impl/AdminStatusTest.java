package hotspotinternet.tlr600.driver.snmp.impl;

import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class AdminStatusTest {

    @Test
    public void testAdminStatusUp() throws Exception {
        assertEquals(
                AdminStatus.UP,
                AdminStatus.fromSnmpValue(1));
    }

    @Test
    public void testAdminStatusDown() throws Exception {
        assertEquals(
                AdminStatus.DOWN,
                AdminStatus.fromSnmpValue(2));
    }

    @Test
    public void testAdminStatusTesting() throws Exception {
        assertEquals(
                AdminStatus.TESTING,
                AdminStatus.fromSnmpValue(3));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAdminStatusUnknown() throws Exception {
        AdminStatus.fromSnmpValue(4);
    }
}
