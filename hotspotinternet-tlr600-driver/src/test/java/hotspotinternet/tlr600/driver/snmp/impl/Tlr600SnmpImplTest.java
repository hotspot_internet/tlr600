package hotspotinternet.tlr600.driver.snmp.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.util.TreeEvent;
import org.snmp4j.util.TreeUtils;

import com.typesafe.config.ConfigFactory;

import hotspotinternet.tlr600.driver.snmp.Tlr600Snmp;


@SuppressWarnings("unchecked")
public class Tlr600SnmpImplTest {
    private final Snmp mockSnmp = mock(Snmp.class);
    private final TreeUtils mockTreeUtils = mock(TreeUtils.class);

    private Tlr600Snmp dut;

    @Before
    public void setupDut() {
        dut = new Tlr600SnmpImpl(
                ConfigFactory.load(),
                transport -> mockSnmp,
                snmp -> mockTreeUtils);
    }

    private void setupGetResponse(
            final OID requestOid,
            final VariableBinding responseBinding) throws Exception {
        ResponseEvent<Address> mockResponseEvent = mockResponseEvent(responseBinding, PDU.GET);
        when(mockSnmp.get(
                argThat(new ArgumentMatcher<PDU>() {
                        @Override
                        public boolean matches(PDU pdu) {
                            return pdu.getVariableBindings().get(0).getOid().equals(requestOid);
                        }
                }),
                any()))
                .thenReturn(mockResponseEvent);
    }

    private void setupSetResponse(
            final OID requestOid,
            final VariableBinding responseBinding) throws Exception {
        ResponseEvent<Address> mockResponseEvent = mockResponseEvent(responseBinding, PDU.SET);
        when(mockSnmp.set(
                argThat(new ArgumentMatcher<PDU>() {
                    @Override
                    public boolean matches(PDU pdu) {
                        return pdu.getVariableBindings().get(0).getOid().equals(requestOid);
                    }
                }),
                any()))
                .thenReturn(mockResponseEvent);
    }

    private static ResponseEvent<Address> mockResponseEvent(
            final VariableBinding responseBinding,
            final int pduType) {
        ResponseEvent<Address> mockResponseEvent = mock(ResponseEvent.class);
        PDU responsePdu = new PDU(pduType, Arrays.asList(responseBinding));
        when(mockResponseEvent.getResponse()).thenReturn(responsePdu);
        return mockResponseEvent;
    }

    private void setupWalkResponse(
            final OID requestOid,
            final VariableBinding responseBinding,
            final boolean isError) {
        TreeEvent mockTreeEvent = mock(TreeEvent.class);
        when(mockTreeEvent.isError()).thenReturn(isError);
        when(mockTreeEvent.getVariableBindings()).thenReturn(
                new VariableBinding[] {responseBinding});

        if (responseBinding == null) {
            mockTreeEvent = null;
        }
        when(mockTreeUtils.getSubtree(any(), eq(requestOid))).thenReturn(
                Collections.singletonList(mockTreeEvent));
    }

    private VariableBinding createBinding(final OID oid, final String variableString) {
        return new VariableBinding(oid, new OctetString(variableString));
    }

    // smoke test
    @Test
    public void testOpen() throws Exception {
        dut.open();
    }

    @Test(expected = IOException.class)
    public void testGetException() throws Exception {
        when(mockSnmp.get(any(), any())).thenThrow(IOException.class);
        dut.open();
        dut.get(SnmpConstants.sysName);
    }

    // just a smoke test that things don't blow up
    @Test
    public void testClose() throws Exception {
        dut.close();
        dut.open();
        dut.close();
    }

    @Test
    public void testGet() throws Exception {
        final String responseString = "some string";
        final OID oid = SnmpConstants.sysName;
        setupGetResponse(oid, createBinding(oid, responseString));
        dut.open();
        VariableBinding variableBinding = dut.get(oid);
        assertEquals(responseString, variableBinding.getVariable().toString());
    }

    @Test
    public void testSet() throws Exception {
        final String requestedValue = "some string";
        final String responseValue = "some string";
        final OID oid = SnmpConstants.sysName;
        setupSetResponse(oid, createBinding(oid, responseValue));
        dut.open();
        VariableBinding variableBinding = dut.set(oid, new OctetString(requestedValue));
        assertEquals(responseValue, variableBinding.getVariable().toString());
    }

    @Test
    public void testWalk() throws Exception {
        final String responseString = "some walk string";
        final OID oid = SnmpConstants.sysName;
        dut.open();
        setupWalkResponse(oid, createBinding(oid, responseString), false);
        assertEquals(1, dut.walk(oid).size());
    }

    @Test
    public void testWalkError() throws Exception {
        final OID oid = SnmpConstants.sysName;
        dut.open();
        setupWalkResponse(oid, createBinding(oid, "any string"), true);
        assertTrue(dut.walk(oid).isEmpty());
    }

    @Test
    public void testWalkNullEvent() throws Exception {
        final OID oid = SnmpConstants.sysName;
        dut.open();
        setupWalkResponse(oid, null, false);
        assertTrue(dut.walk(oid).isEmpty());
    }
}
