package hotspotinternet.tlr600.driver.impl;

import static com.google.common.base.Preconditions.checkArgument;

import java.io.IOException;
import java.time.Instant;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.Integer32;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.Variable;
import org.snmp4j.smi.VariableBinding;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import com.google.inject.Inject;
import com.typesafe.config.Config;

import hotspotinternet.tlr600.driver.Tlr600Driver;
import hotspotinternet.tlr600.driver.snmp.Tlr600Snmp;
import hotspotinternet.tlr600.driver.snmp.impl.AdminStatus;
import hotspotinternet.tlr600.driver.snmp.impl.IfExTableColumn;
import hotspotinternet.tlr600.driver.snmp.impl.InterfacesColumn;
import hotspotinternet.tlr600.driver.snmp.impl.OperStatus;

/**
 * Implementation of driver interface for interacting with TP-Link Tlr600VPN.
 */
public class Tlr600DriverImpl implements Tlr600Driver {
    private static final Logger LOG = LoggerFactory.getLogger(Tlr600DriverImpl.class);
    private static final String INTERFACES_KEY = "tlr600.wan.interfaces";
    private static final String INTERFACE_ADDRESS_KEY = "tlr600.interface.address";
    // Where "Label" is the index labeling the interface on the actual physical hardware
    // This is different from the "SNMP Index", which is the index used to represent the interface
    // in a column of the IF-MIB interfaces table
    private final ImmutableMap<String, Integer> addressToLabelMap;
    private final Tlr600Snmp snmp;
    private final Supplier<Instant> instantSupplier;
    private final List<Integer> wanInterfaces;

    @Inject
    Tlr600DriverImpl(
            final Config config,
            final Supplier<Instant> instantSupplier,
            final Tlr600Snmp snmp) {
        this.instantSupplier = instantSupplier;
        this.snmp = snmp;
        wanInterfaces = config.getIntList(INTERFACES_KEY);
        addressToLabelMap = config.getConfig(INTERFACE_ADDRESS_KEY)
                .entrySet()
                .stream()
                .collect(ImmutableMap.toImmutableMap(
                        entry -> entry.getValue().unwrapped().toString(),
                        entry -> Integer.valueOf(entry.getKey())));
        checkArgument(
                wanInterfaces
                    .stream()
                    .allMatch(addressToLabelMap::containsValue),
                String.format(
                        "Only interfaces with a '%s' entry allowed in '%s",
                        INTERFACE_ADDRESS_KEY,
                        INTERFACES_KEY));
    }

    @Override
    public void close() throws IOException {
        snmp.close();
    }

    @Override
    public void open() throws IOException {
        snmp.open();
        VariableBinding variableBinding = snmp.get(SnmpConstants.sysName);
        LOG.debug("System name: {}", variableBinding.getVariable().toString());
    }

    @Override
    public List<InterfaceReading> getInterfaceReadings() {
        return createReadingsBuilder()
                .operStatus(getSnmpInterfaceReadings(
                        InterfacesColumn.IF_OPER_STATUS,
                        variable -> OperStatus.fromSnmpValue(variable.toInt())))
                .inBytes(getSnmpInterfaceLongs(
                        IfExTableColumn.IF_HC_IN_OCTETS))
                .inDiscards(getSnmpInterfaceLongs(
                        InterfacesColumn.IF_IN_DISCARDS))
                .inErrors(getSnmpInterfaceLongs(
                        InterfacesColumn.IF_IN_ERRORS))
                .inMulticastPackets(getSnmpInterfaceLongs(
                        IfExTableColumn.IF_HC_IN_MULTICAST_PKTS))
                .inUnicastPackets(getSnmpInterfaceLongs(
                        IfExTableColumn.IF_HC_IN_UCAST_PKTS))
                .inUnknownProtocols(getSnmpInterfaceLongs(
                        InterfacesColumn.IF_IN_UNKNOWN_PROTOS))
                .outBytes(getSnmpInterfaceLongs(
                        IfExTableColumn.IF_HC_OUT_OCTETS))
                .outDiscards(getSnmpInterfaceLongs(
                        InterfacesColumn.IF_OUT_DISCARDS))
                .outErrors(getSnmpInterfaceLongs(
                        InterfacesColumn.IF_OUT_ERRORS))
                .outMulticastPackets(getSnmpInterfaceLongs(
                        IfExTableColumn.IF_HC_OUT_MULTICAST_PKTS))
                .outUnicastPackets(getSnmpInterfaceLongs(
                        IfExTableColumn.IF_HC_OUT_UCAST_PKTS))
                .build()
                .stream()
                .map(SnmpInterfaceReading::getInterfaceReading)
                .collect(Collectors.toList());
    }

    private InterfaceReadingsBuilder createReadingsBuilder() {
        return new InterfaceReadingsBuilder(
                instantSupplier,
                getSnmpInterfaceReadings(
                        InterfacesColumn.IF_ADMIN_STATUS,
                        variable -> AdminStatus.fromSnmpValue(variable.toInt())),
                getLabelIfIndexToSnmpIndices());
    }

    private ImmutableMap<Integer, Set<Integer>> getLabelIfIndexToSnmpIndices() {
        // An interface address has been observed to have multiple entries in the SNMP IF-MIB
        // interfaces table; therefore, we need a Set to represent all of the entries for
        // a physical interface. This is necessary since for some reading fields (e.g. octets
        // out) the resultant for the interface is sum of these entries from interfaces table
        final ImmutableMap<Integer, String> snmpIndexToAddressMap = getSnmpInterfaceReadings(
                InterfacesColumn.IF_PHYS_ADDRESS,
                Variable::toString);
        final Map<String, Set<Integer>> ifAddressToSnmpIndices =
                new HashMap<String, Set<Integer>>();
        for (Entry<Integer, String> entry : snmpIndexToAddressMap.entrySet()) {
            String address = entry.getValue();
            int snmpIndex = entry.getKey();
            ifAddressToSnmpIndices.merge(
                    address,
                    Collections.singleton(snmpIndex),
                    Sets::union);
        }
        final ImmutableMap.Builder<Integer, Set<Integer>> labelIfIndexToSnmpIndicesBuilder =
                new ImmutableMap.Builder<>();
        for (Entry<String, Integer> entry : addressToLabelMap.entrySet()) {
            final String expectedIfaceAddress = entry.getKey();
            final int ifaceLabelIndex = entry.getValue();
            Set<Integer> snmpIfIndices = ifAddressToSnmpIndices.get(expectedIfaceAddress);
            if (snmpIfIndices == null) {
                LOG.warn(
                        "No SNMP interfaces for provided MAC address: {}",
                        expectedIfaceAddress);
                continue;
            } else if (snmpIfIndices.size() > 1) {
                LOG.warn("{} SNMP indices found for MAC {} (interface label = {})",
                        snmpIfIndices.size(),
                        expectedIfaceAddress,
                        ifaceLabelIndex);
            }
            labelIfIndexToSnmpIndicesBuilder.put(ifaceLabelIndex, snmpIfIndices);
        }
        return labelIfIndexToSnmpIndicesBuilder.build();
    }

    private ImmutableMap<Integer, Long> getSnmpInterfaceLongs(
            final Supplier<OID> columnOidSupplier) {
        return getSnmpInterfaceReadings(columnOidSupplier, Variable::toLong);
    }

    private <T> ImmutableMap<Integer, T> getSnmpInterfaceReadings(
            final Supplier<OID> columnOidSupplier,
            final Function<Variable, T> readingFunction) {
        return getInterfacesColumnReadings(columnOidSupplier)
                .entrySet()
                .stream()
                .collect(ImmutableMap.toImmutableMap(
                        Entry::getKey,
                        entry -> readingFunction.apply(entry.getValue())));
    }

    private Map<Integer, Variable> getInterfacesColumnReadings(
            final Supplier<OID> columnOidSupplier) {
        final OID columnOid = columnOidSupplier.get();
        return snmp.walk(columnOid)
                .stream()
                .collect(Collectors.toMap(
                        binding -> binding.getOid().getSuffix(columnOid).getValue()[0],
                        VariableBinding::getVariable));
    }

    @Override
    public void setInterfaceEnabled(int interfaceIndex, boolean isEnabled) throws IOException {
        int activeSnmpIndex = createReadingsBuilder()
                .build()
                .stream()
                .filter(reading ->
                        reading.getInterfaceReading().getInterfaceIndex() == interfaceIndex)
                .map(SnmpInterfaceReading::getSnmpIndex)
                .findAny()
                .orElseThrow(() ->
                        new IllegalArgumentException("No interface index of " + interfaceIndex));

        AdminStatus adminStatus = isEnabled ? AdminStatus.UP : AdminStatus.DOWN;
        VariableBinding variableBinding = snmp.set(
                InterfacesColumn.IF_ADMIN_STATUS.get(activeSnmpIndex),
                new Integer32(adminStatus.getSnmpValue()));
        LOG.debug("VariableBinding response to set: {}", variableBinding);
    }

    @Override
    public List<Integer> getWanInterfaces() {
        return wanInterfaces;
    }
}
