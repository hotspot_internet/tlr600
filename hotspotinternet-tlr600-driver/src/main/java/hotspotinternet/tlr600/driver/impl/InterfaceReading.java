package hotspotinternet.tlr600.driver.impl;

import java.time.Instant;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

import hotspotinternet.tlr600.driver.snmp.impl.AdminStatus;
import hotspotinternet.tlr600.driver.snmp.impl.OperStatus;

/**
 * Class for storing information of an interface reading.
 */
public class InterfaceReading {

    private Integer interfaceIndex;
    private Long measurementId;
    private Instant instant;
    private AdminStatus adminStatus;
    private OperStatus operStatus;
    private Long inBytes;
    private Long inDiscards;
    private Long inErrors;
    private Long inMulticastPackets;
    private Long inUnicastPackets;
    private Long inUnknownProtocols;
    private Long outDiscards;
    private Long outErrors;
    private Long outMulticastPackets;
    private Long outUnicastPackets;
    private Long outBytes;

    /**
     * Default constructor.
     */
    public InterfaceReading() {}

    /**
     * Construct InterfaceReading with provided arguments.
     *
     * @param measurementId measurement ID
     * @param interfaceIndex index of interface
     * @param instant when reading taken
     * @param adminStatus admin status
     * @param operStatus operation status
     * @param inBytes number of bytes received
     * @param inDiscards in-packets discarded
     * @param inErrors in-packet errors
     * @param inMulticastPackets multicast packets received
     * @param inUnicastPackets unicast packets received
     * @param inUnknownProtocols packets received with unknown protocol
     * @param outBytes number of bytes sent out
     * @param outDiscards out-packets discarded
     * @param outErrors out-packet errors
     * @param outMulticastPackets multicast packets sent
     * @param outUnicastPackets unicast packets sent
     */
    @VisibleForTesting
    public InterfaceReading(
            final Long measurementId,
            final int interfaceIndex,
            final Instant instant,
            final AdminStatus adminStatus,
            final OperStatus operStatus,
            final Long inBytes,
            final Long inDiscards,
            final Long inErrors,
            final Long inMulticastPackets,
            final Long inUnicastPackets,
            final Long inUnknownProtocols,
            final Long outBytes,
            final Long outDiscards,
            final Long outErrors,
            final Long outMulticastPackets,
            final Long outUnicastPackets) {
        this.interfaceIndex = interfaceIndex;
        this.instant = instant;
        this.measurementId = measurementId;
        this.adminStatus = adminStatus;
        this.operStatus = operStatus;
        this.inBytes = inBytes;
        this.inDiscards = inDiscards;
        this.inErrors = inErrors;
        this.inMulticastPackets = inMulticastPackets;
        this.inUnicastPackets = inUnicastPackets;
        this.inUnknownProtocols = inUnknownProtocols;
        this.outBytes = outBytes;
        this.outDiscards = outDiscards;
        this.outErrors = outErrors;
        this.outMulticastPackets = outMulticastPackets;
        this.outUnicastPackets = outUnicastPackets;
    }

    InterfaceReading(final int interfaceIndex, final Instant instant) {
        this.interfaceIndex = interfaceIndex;
        this.instant = instant;
    }

    /**
     * Gets measurement ID.
     *
     * @return measurement ID
     */
    public Long getMeasurementId() {
        return measurementId;
    }

    /**
     * Gets interface index.
     *
     * @return interface index
     */
    public Integer getInterfaceIndex() {
        return interfaceIndex;
    }

    /**
     * Gets instant for when reading was taken.
     *
     * @return the instant of reading
     */
    public Instant getInstant() {
        return instant;
    }

    /**
     * Gets admin status.
     *
     * @return the adminStatus
     */
    public AdminStatus getAdminStatus() {
        return adminStatus;
    }

    /**
     * Gets operational status.
     *
     * @return the operStatus
     */
    public OperStatus getOperStatus() {
        return operStatus;
    }

    /**
     * Gets number of bytes received.
     *
     * @return bytes received
     */
    public Long getInBytes() {
        return inBytes;
    }

    /**
     * Gets number of in packets discarded.
     *
     * @return the inDiscards
     */
    public Long getInDiscards() {
        return inDiscards;
    }

    /**
     * Gets number of error packets received.
     *
     * @return the inErrors
     */
    public Long getInErrors() {
        return inErrors;
    }

    /**
     * Gets number of multicast packets received.
     *
     * @return the inMulticastPackets
     */
    public Long getInMulticastPackets() {
        return inMulticastPackets;
    }

    /**
     * Gets number of unicast packets received.
     *
     * @return the inUnicastPackets
     */
    public Long getInUnicastPackets() {
        return inUnicastPackets;
    }

    /**
     * Gets number of packets received with unknown protocols.
     *
     * @return the inUnknownProtocols
     */
    public Long getInUnknownProtocols() {
        return inUnknownProtocols;
    }

    /**
     * Gets number of bytes sent out.
     *
     * @return bytes sent out
     */
    public Long getOutBytes() {
        return outBytes;
    }

    /**
     * Gets number of out-packets discarded.
     *
     * @return the outDiscards
     */
    public Long getOutDiscards() {
        return outDiscards;
    }

    /**
     * Gets number of error out-packet errors.
     *
     * @return the outErrors
     */
    public Long getOutErrors() {
        return outErrors;
    }

    /**
     * Gets number of multicast packets sent.
     *
     * @return the outMulticastPackets
     */
    public Long getOutMulticastPackets() {
        return outMulticastPackets;
    }

    /**
     * Gets number of unicast packets sent.
     *
     * @return the outUnicastPackets
     */
    public Long getOutUnicastPackets() {
        return outUnicastPackets;
    }

    void setAdminStatus(final AdminStatus adminStatus) {
        this.adminStatus = adminStatus;
    }

    void setOperStatus(final OperStatus operStatus) {
        this.operStatus = operStatus;
    }

    void setInBytes(final Long inBytes) {
        this.inBytes = inBytes;
    }

    void setInDiscards(final Long inDiscards) {
        this.inDiscards = inDiscards;
    }

    void setInErrors(final Long inErrors) {
        this.inErrors = inErrors;
    }

    void setInMulticastPackets(final Long inMulticastPackets) {
        this.inMulticastPackets = inMulticastPackets;
    }

    void setInUnicastPackets(final Long inUnicastPackets) {
        this.inUnicastPackets = inUnicastPackets;
    }

    void setInUnknownProtocols(final Long inUnknownProtocols) {
        this.inUnknownProtocols = inUnknownProtocols;
    }

    void setOutBytes(final Long outBytes) {
        this.outBytes = outBytes;
    }

    void setOutDiscards(final Long outDiscards) {
        this.outDiscards = outDiscards;
    }

    void setOutErrors(final Long outErrors) {
        this.outErrors = outErrors;
    }

    void setOutMulticastPackets(final Long outMulticastPackets) {
        this.outMulticastPackets = outMulticastPackets;
    }

    void setOutUnicastPackets(final Long outUnicastPackets) {
        this.outUnicastPackets = outUnicastPackets;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("Measurement Id", measurementId)
                .add("Interface Index", interfaceIndex)
                .add("Instant", instant)
                .add("Admin Status", adminStatus)
                .add("Oper Status", operStatus)
                .add("In Bytes", inBytes)
                .add("In Discards", inDiscards)
                .add("In Errors", inErrors)
                .add("In Multicast Packets", inMulticastPackets)
                .add("In Unicast Packets", inUnicastPackets)
                .add("In Unknown Protocols", inUnknownProtocols)
                .add("Out Bytes", outBytes)
                .add("Out Discards", outDiscards)
                .add("Out Errors", outErrors)
                .add("Out Multicast Packets", outMulticastPackets)
                .add("Out Unicast Packets", outUnicastPackets)
                .toString();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof InterfaceReading)) {
            return false;
        }
        InterfaceReading otherInterfaceReading = (InterfaceReading) other;
        return Objects.equal(measurementId, otherInterfaceReading.measurementId)
                && Objects.equal(interfaceIndex, otherInterfaceReading.interfaceIndex)
                && Objects.equal(instant, otherInterfaceReading.instant)
                && Objects.equal(adminStatus, otherInterfaceReading.adminStatus)
                && Objects.equal(operStatus, otherInterfaceReading.operStatus)
                && Objects.equal(inBytes, otherInterfaceReading.inBytes)
                && Objects.equal(inDiscards, otherInterfaceReading.inDiscards)
                && Objects.equal(inErrors, otherInterfaceReading.inErrors)
                && Objects.equal(inMulticastPackets, otherInterfaceReading.inMulticastPackets)
                && Objects.equal(inUnicastPackets, otherInterfaceReading.inUnicastPackets)
                && Objects.equal(inUnknownProtocols, otherInterfaceReading.inUnknownProtocols)
                && Objects.equal(outBytes, otherInterfaceReading.outBytes)
                && Objects.equal(outDiscards, otherInterfaceReading.outDiscards)
                && Objects.equal(outErrors, otherInterfaceReading.outErrors)
                && Objects.equal(outMulticastPackets, otherInterfaceReading.outMulticastPackets)
                && Objects.equal(outUnicastPackets, otherInterfaceReading.outUnicastPackets);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(
                measurementId,
                interfaceIndex,
                instant,
                adminStatus,
                operStatus,
                inBytes,
                inDiscards,
                inErrors,
                inMulticastPackets,
                inUnicastPackets,
                inUnknownProtocols,
                outBytes,
                outDiscards,
                outErrors,
                outMulticastPackets,
                outUnicastPackets);
    }
}
