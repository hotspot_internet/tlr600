package hotspotinternet.tlr600.driver.impl;

/**
 * Defines the SNMP index that is currently active for a physical interface
 * and the associated InterfaceReading at a given instant. Note that multiple
 * SNMP indices may be associated with a given physical interface, but only
 * one of them would be active.
 */
class SnmpInterfaceReading {
    private final int snmpIndex;
    private final InterfaceReading interfaceReading;

    SnmpInterfaceReading(final int activeSnmpIndex, final InterfaceReading interfaceReading) {
        this.snmpIndex = activeSnmpIndex;
        this.interfaceReading = interfaceReading;
    }

    int getSnmpIndex() {
        return snmpIndex;
    }

    InterfaceReading getInterfaceReading() {
        return interfaceReading;
    }
}
