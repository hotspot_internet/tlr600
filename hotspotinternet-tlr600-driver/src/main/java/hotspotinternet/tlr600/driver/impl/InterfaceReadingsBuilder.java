package hotspotinternet.tlr600.driver.impl;

import java.time.Instant;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableMap;

import hotspotinternet.tlr600.driver.snmp.impl.AdminStatus;
import hotspotinternet.tlr600.driver.snmp.impl.OperStatus;

/**
 * Class for building a list of interface readings from results of SNMP queries.
 */
class InterfaceReadingsBuilder {

    private final ImmutableMap<Integer, SnmpInterfaceReading> labelIfIndexToReadingMap;
    private final ImmutableMap<Integer, Set<Integer>> labelIfIndexToSnmpIndices;

    /**
     * Constructs InterfaceReadingsBuilder from provided arguments.
     *
     * @param instantSupplier for setting instant of when reading occurred
     * @param snmpIndexToAdminStatus for setting admin status and determining active SNMP index
     * @param labelIfIndexToSnmpIndices map of interface label index to SNMP interface
     *      indices
     */
    public InterfaceReadingsBuilder(
            final Supplier<Instant> instantSupplier,
            final Map<Integer, AdminStatus> snmpIndexToAdminStatus,
            final ImmutableMap<Integer, Set<Integer>> labelIfIndexToSnmpIndices) {
        this.labelIfIndexToSnmpIndices = labelIfIndexToSnmpIndices;
        labelIfIndexToReadingMap = labelIfIndexToSnmpIndices
                .keySet()
                .stream()
                .collect(ImmutableMap.toImmutableMap(
                        Function.identity(),
                        interfaceIndex -> new SnmpInterfaceReading(
                                getActiveSnmpIndex(
                                        labelIfIndexToSnmpIndices.get(interfaceIndex),
                                        snmpIndexToAdminStatus),
                                new InterfaceReading(interfaceIndex, instantSupplier.get()))));
        adminStatus(snmpIndexToAdminStatus);
    }

    private static int getActiveSnmpIndex(
            final Set<Integer> snmpIndices,
            final Map<Integer, AdminStatus> snmpIndexToAdminStatus) {
        List<Integer> adminUpSnmpIndices = snmpIndices
                .stream()
                .filter(snmpIndex -> snmpIndexToAdminStatus.get(snmpIndex) == AdminStatus.UP)
                .collect(Collectors.toList());
        Collection<Integer> eligibleActiveSnmpIndices = !adminUpSnmpIndices.isEmpty()
                ? adminUpSnmpIndices
                : snmpIndices;
        return eligibleActiveSnmpIndices
                .stream()
                .sorted(Comparator.reverseOrder())
                .findFirst()
                .get();
    }

    private InterfaceReadingsBuilder adminStatus(
            final Map<Integer, AdminStatus> snmpIndexToAdminStatus) {
        return setInstantaneousField(snmpIndexToAdminStatus, InterfaceReading::setAdminStatus);
    }

    InterfaceReadingsBuilder operStatus(final Map<Integer, OperStatus> snmpIndexToOperStatus) {
        return setInstantaneousField(snmpIndexToOperStatus, InterfaceReading::setOperStatus);
    }

    InterfaceReadingsBuilder inBytes(final Map<Integer, Long> snmpIndexToInOctets) {
        return setLongField(snmpIndexToInOctets, InterfaceReading::setInBytes);
    }

    InterfaceReadingsBuilder inDiscards(
            final Map<Integer, Long> snmpIndexToInDiscards) {
        return setLongField(snmpIndexToInDiscards, InterfaceReading::setInDiscards);
    }

    InterfaceReadingsBuilder inErrors(
            final Map<Integer, Long> snmpIndexToInErrors) {
        return setLongField(snmpIndexToInErrors, InterfaceReading::setInErrors);
    }

    InterfaceReadingsBuilder inMulticastPackets(
            final Map<Integer, Long> snmpIndexToInMulticastPackets) {
        return setLongField(
                snmpIndexToInMulticastPackets,
                InterfaceReading::setInMulticastPackets);
    }

    InterfaceReadingsBuilder inUnicastPackets(
            final Map<Integer, Long> snmpIndexToInUcastPackets) {
        return setLongField(
                snmpIndexToInUcastPackets,
                InterfaceReading::setInUnicastPackets);
    }

    InterfaceReadingsBuilder inUnknownProtocols(
            final Map<Integer, Long> snmpIndexToInUnknownProtocols) {
        return setLongField(
                snmpIndexToInUnknownProtocols,
                InterfaceReading::setInUnknownProtocols);
    }

    InterfaceReadingsBuilder outBytes(final Map<Integer, Long> snmpIndexToOutOctets) {
        return setLongField(snmpIndexToOutOctets, InterfaceReading::setOutBytes);
    }

    InterfaceReadingsBuilder outDiscards(
            final Map<Integer, Long> snmpIndexToOutDiscards) {
        return setLongField(snmpIndexToOutDiscards, InterfaceReading::setOutDiscards);
    }

    InterfaceReadingsBuilder outErrors(
            final Map<Integer, Long> snmpIndexToOutErrors) {
        return setLongField(snmpIndexToOutErrors, InterfaceReading::setOutErrors);
    }

    InterfaceReadingsBuilder outMulticastPackets(
            final Map<Integer, Long> snmpIndexToOutnMulticastPackets) {
        return setLongField(
                snmpIndexToOutnMulticastPackets,
                InterfaceReading::setOutMulticastPackets);
    }

    InterfaceReadingsBuilder outUnicastPackets(
            final Map<Integer, Long> snmpIndexToOutUcastPackets) {
        return setLongField(
                snmpIndexToOutUcastPackets,
                InterfaceReading::setOutUnicastPackets);
    }

    private <T> InterfaceReadingsBuilder setInstantaneousField(
            final Map<Integer, T> snmpIndexToValue,
            final BiConsumer<InterfaceReading, T> readingFieldSetter) {
        for (Entry<Integer, Set<Integer>> entry : labelIfIndexToSnmpIndices.entrySet()) {
            final int labelIfIndex = entry.getKey();
            final Set<Integer> snmpIndices = entry.getValue();
            if (snmpIndices == null) {
                continue;
            }
            final SnmpInterfaceReading snmpInterfaceReading =
                    labelIfIndexToReadingMap.get(labelIfIndex);
            readingFieldSetter.accept(
                    snmpInterfaceReading.getInterfaceReading(),
                    snmpIndexToValue.get(snmpInterfaceReading.getSnmpIndex()));
        }
        return this;
    }

    private InterfaceReadingsBuilder setLongField(
            final Map<Integer, Long> snmpIndexToVariable,
            final BiConsumer<InterfaceReading, Long> readingFieldSetter) {
        return setField(
                snmpIndexToVariable,
                collection -> collection.stream().reduce(0L, Long::sum),
                readingFieldSetter);
    }

    private <T> InterfaceReadingsBuilder setField(
            final Map<Integer, T> snmpIndexToVariable,
            final Function<Collection<T>, T> aggregator,
            final BiConsumer<InterfaceReading, T> readingFieldSetter) {
        for (Entry<Integer, Set<Integer>> entry : labelIfIndexToSnmpIndices.entrySet()) {
            final int labelIfIndex = entry.getKey();
            final Set<Integer> snmpIndices = entry.getValue();
            if (snmpIndices == null) {
                continue;
            }
            readingFieldSetter.accept(
                    labelIfIndexToReadingMap.get(labelIfIndex).getInterfaceReading(),
                    aggregator.apply(
                            snmpIndices
                            .stream()
                            .sorted()
                            .map(snmpIndexToVariable::get)
                            .collect(Collectors.toList())));
        }
        return this;
    }

    List<SnmpInterfaceReading> build() {
        return labelIfIndexToReadingMap
                .values()
                .stream()
                .collect(Collectors.toList());
    }
}
