package hotspotinternet.tlr600.driver.impl;

import java.time.Instant;
import java.util.function.Function;
import java.util.function.Supplier;

import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.smi.Address;
import org.snmp4j.util.DefaultPDUFactory;
import org.snmp4j.util.TreeUtils;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;

import hotspotinternet.tlr600.driver.Tlr600Driver;
import hotspotinternet.tlr600.driver.snmp.Tlr600Snmp;
import hotspotinternet.tlr600.driver.snmp.impl.Tlr600SnmpImpl;

/**
 * Guice module for binding driver implementations for interfaces.
 */
public class Tlr600DriverModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(Tlr600Driver.class).to(Tlr600DriverImpl.class);

        // bindings for Tlr600DriverImpl constructor
        bind(Tlr600Snmp.class).to(Tlr600SnmpImpl.class);
        bind(new TypeLiteral<Supplier<Instant>>(){}).toInstance(Instant::now);

        // bindings for Tlr600SnmpImpl constructor
        bind(new TypeLiteral<Function<TransportMapping<? extends Address>, Snmp>>(){})
                .toInstance(transport -> new Snmp(transport));
        bind(new TypeLiteral<Function<Snmp, TreeUtils>>(){})
                .toInstance(snmp -> new TreeUtils(snmp, new DefaultPDUFactory()));
    }
}
