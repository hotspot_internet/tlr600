package hotspotinternet.tlr600.driver.snmp.impl;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.Integer32;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.Variable;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultUdpTransportMapping;
import org.snmp4j.util.TreeEvent;
import org.snmp4j.util.TreeUtils;

import com.google.inject.Inject;
import com.typesafe.config.Config;

import hotspotinternet.tlr600.driver.snmp.Tlr600Snmp;

/**
 * Implementation of interface for interacting with TP-Link Tlr600VPN router via SNMP.
 */
public class Tlr600SnmpImpl implements Tlr600Snmp {
    private static final Logger LOG = LoggerFactory.getLogger(Tlr600SnmpImpl.class);
    // 'private' community allows setting values via SNMP
    private static final String COMMUNITY = "private";
    private static final String SNMP_TARGET_KEY = "tlr600.snmp.target";

    private final CommunityTarget<Address> target;
    private final Function<TransportMapping<? extends Address>, Snmp> snmpFunction;
    private final Function<Snmp, TreeUtils> treeUtilsFunction;

    private Snmp snmp;
    private TreeUtils treeUtils;
    private TransportMapping<? extends Address> transport;

    @Inject
    Tlr600SnmpImpl(
            final Config config,
            final Function<TransportMapping<? extends Address>, Snmp> snmpFunction,
            final Function<Snmp, TreeUtils> treeUtilsFunction) {
        this.snmpFunction = snmpFunction;
        this.treeUtilsFunction = treeUtilsFunction;
        target = createTarget(config.getString(SNMP_TARGET_KEY));
    }

    private CommunityTarget<Address> createTarget(final String targetAddress) {
        CommunityTarget<Address> target = new CommunityTarget<Address>();
        target.setCommunity(new OctetString(COMMUNITY));
        target.setAddress(GenericAddress.parse(targetAddress));
        target.setRetries(2);
        target.setTimeout(1500);
        target.setVersion(SnmpConstants.version2c);
        return target;
    }

    @Override
    public void open() throws IOException {
        connect();
    }

    private void connect() throws IOException {
        LOG.debug("Connecting");
        transport = new DefaultUdpTransportMapping();
        snmp = snmpFunction.apply(transport);
        treeUtils = treeUtilsFunction.apply(snmp);
        transport.listen();
        LOG.debug("Connected");
    }

    @Override
    public VariableBinding get(OID oid) throws IOException {
        PDU pdu = new PDU();
        pdu.add(new VariableBinding(SnmpConstants.sysName));
        pdu.setType(PDU.GET);
        pdu.setRequestID(new Integer32(1));
        ResponseEvent<Address> response = snmp.get(pdu, target);
        List<? extends VariableBinding> variableBindings =
                response.getResponse().getVariableBindings();

        if (variableBindings.isEmpty()) {
            throw new IOException(
                    "No variable bindings received in response for provided OID of " + oid);
        }
        if (variableBindings.size() > 1) {
            LOG.warn("Expected 1 variable binding but recevied {} in response for OID {}",
                    variableBindings.size(),
                    oid);
        }
        return variableBindings.get(0);
    }

    @Override
    public VariableBinding set(OID oid, Variable variable) throws IOException {
        PDU pdu = new PDU();
        pdu.add(new VariableBinding(oid, variable));
        pdu.setType(PDU.SET);
        ResponseEvent<Address> response = snmp.set(pdu, target);
        List<? extends VariableBinding> variableBindings =
                response.getResponse().getVariableBindings();

        if (variableBindings.isEmpty()) {
            throw new IOException(
                    "No variable bindings received in response for provided OID of " + oid);
        }
        if (variableBindings.size() > 1) {
            LOG.warn("Expected 1 variable binding but recevied {} in response for OID {}",
                    variableBindings.size(),
                    oid);
        }
        return variableBindings.get(0);
    }

    @Override
    public List<VariableBinding> walk(final OID oid) {
        List<VariableBinding> variableBindings = new ArrayList<>();
        List<TreeEvent> events = treeUtils.getSubtree(target, oid);
        if (events == null || events.size() == 0) {
            LOG.warn("Error: Unable to read table...");
            return variableBindings;
        }
        for (TreeEvent event : events) {
            if (event == null) {
                LOG.warn("Null event for table OID [{}]", oid);
                continue;
            }
            if (event.isError()) {
                LOG.warn("Error for table OID [{}]: {}", oid, event.getErrorMessage());
                continue;
            }

            VariableBinding[] varBindings = event.getVariableBindings();
            if (varBindings == null || varBindings.length == 0) {
                LOG.warn("No bindings for table OID [{}]", oid);
                continue;
            }
            variableBindings.addAll(Arrays.asList(varBindings));
        }
        return variableBindings;
    }

    @Override
    public void close() throws IOException {
        if (transport != null) {
            LOG.debug("Closing");
            transport.close();
            LOG.debug("Closed");
        }
    }
}
