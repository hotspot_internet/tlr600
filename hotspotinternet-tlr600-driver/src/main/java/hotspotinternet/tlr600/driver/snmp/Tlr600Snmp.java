package hotspotinternet.tlr600.driver.snmp;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;

import org.snmp4j.smi.OID;
import org.snmp4j.smi.Variable;
import org.snmp4j.smi.VariableBinding;

/**
 * Interface for interacting with TP-Link Tlr600VPN router via SNMP.
 */
public interface Tlr600Snmp extends Closeable {
    /**
     * Opens for SNMP communications.
     *
     * @throws IOException if problem initiating SNMP comms
     */
    void open() throws IOException;

    /**
     * Sends GET request for provided OID and returns variable binding of response.
     *
     * @param oid OID of GET request
     * @return VariableBinding of response
     * @throws IOException if problem sending request
     */
    VariableBinding get(OID oid) throws IOException;

    /**
     * Sends SET request for setting an OID to a Variable.
     *
     * @param oid OID of SET request
     * @param variable Variable to be set for the OID
     * @return VariableBinding of response
     * @throws IOException if problem sending request
     */
    VariableBinding set(OID oid, Variable variable) throws IOException;

    /**
     * Walks SNMP subfields for provided oid.
     *
     * @param oid OID to walk
     * @return List of variable bindings for all SNMP subfields in tree
     */
    List<VariableBinding> walk(final OID oid);
}
