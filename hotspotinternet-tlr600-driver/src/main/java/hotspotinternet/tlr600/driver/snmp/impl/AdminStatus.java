package hotspotinternet.tlr600.driver.snmp.impl;

import java.util.Arrays;

/**
 * Enumerations for column ifAdminStatus of SNMP IF-MIB 'interfaces' table.
 */
public enum AdminStatus {
    /** UP. */
    UP(1),
    /** DOWN. */
    DOWN(2),
    /** TESTING. */
    TESTING(3);

    private final int snmpValue;

    private AdminStatus(final int snmpValue) {
        this.snmpValue = snmpValue;
    }

    /**
     * Gets AdminStatus corresponding to provided SNMP value.
     *
     * @param snmpValue integer corresponding to SNMP AdminStatus
     * @return AdminStatus corresponding to provided SNMP value
     * @throws IllegalArgumentException if provided snmpValue does not map to an AdminStatus
     */
    public static AdminStatus fromSnmpValue(final int snmpValue) {
        return Arrays.asList(values())
                .stream()
                .filter(adminStatus -> adminStatus.snmpValue == snmpValue)
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException(String.format(
                        "Received SNMP value of %s does not map to admin status",
                        snmpValue)));
    }

    public int getSnmpValue() {
        return snmpValue;
    }
}
