package hotspotinternet.tlr600.driver.snmp.impl;

import java.util.function.Supplier;

import org.snmp4j.smi.OID;

/**
 * Columns within the SNMP 'interfaces' table of IF-MIB.
 */
public enum InterfacesColumn implements Supplier<OID> {
    /** IF_INDEX. */
    IF_INDEX(1),
    /** IF_DESCR. */
    IF_DESCR(2),
    /** IF_TYPE. */
    IF_TYPE(3),
    /** IF_MTU. */
    IF_MTU(4),
    /** IF_SPEED. */
    IF_SPEED(5),
    /** IF_PHYS_ADDRESS. */
    IF_PHYS_ADDRESS(6),
    /** IF_ADMIN_STATUS. */
    IF_ADMIN_STATUS(7),
    /** IF_OPER_STATUS. */
    IF_OPER_STATUS(8),
    /** IF_LAST_CHANGE. */
    IF_LAST_CHANGE(9),
    /** IF_IN_OCTETS. */
    IF_IN_OCTETS(10),
    /** IF_IN_UCAST_PKTS. */
    IF_IN_UCAST_PKTS(11),
    /** IF_IN_NUCAST_PKTS. */
    IF_IN_NUCAST_PKTS(12),
    /** IF_IN_DISCARDS. */
    IF_IN_DISCARDS(13),
    /** IF_IN_ERRORS. */
    IF_IN_ERRORS(14),
    /** IF_IN_UNKNOWN_PROTOS. */
    IF_IN_UNKNOWN_PROTOS(15),
    /** IF_OUT_OCTETS. */
    IF_OUT_OCTETS(16),
    /** IF_OUT_UCAST_PKTS. */
    IF_OUT_UCAST_PKTS(17),
    /** IF_OUT_NUCAST_PKTS. */
    IF_OUT_NUCAST_PKTS(18),
    /** IF_OUT_DISCARDS. */
    IF_OUT_DISCARDS(19),
    /** IF_OUT_ERRORS. */
    IF_OUT_ERRORS(20),
    /** IF_OUT_QLEN. */
    IF_OUT_QLEN(21),
    /** IF_SPECIFIC. */
    IF_SPECIFIC(22);

    private static final OID IFACES_TABLE_OID = new OID(new int[] {1, 3, 6, 1, 2, 1, 2, 2, 1});

    private final int columnIndex;

    private InterfacesColumn(final int columnIndex) {
        this.columnIndex = columnIndex;
    }

    @Override
    public OID get() {
        return new OID(IFACES_TABLE_OID.getValue(), columnIndex);
    }

    public OID get(final int ifIndex) {
        return new OID(get().getValue(), ifIndex);
    }
}
