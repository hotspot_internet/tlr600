package hotspotinternet.tlr600.driver.snmp.impl;

import java.util.function.Supplier;

import org.snmp4j.smi.OID;

/**
 * Columns within the SNMP 'IfXTable' of IF-MIB.
 */
public enum IfExTableColumn implements Supplier<OID> {
    /** IF_NAME. */
    IF_NAME(1),
    /** IF_IN_MULTICAST_PKTS. */
    IF_IN_MULTICAST_PKTS(2),
    /** IF_IN_BROADCAST_PKTS. */
    IF_IN_BROADCAST_PKTS(3),
    /** IF_OUT_MULTICAST_PKTS. */
    IF_OUT_MULTICAST_PKTS(4),
    /** IF_OUT_BROADCAST_PKTS. */
    IF_OUT_BROADCAST_PKTS(5),
    /** IF_HC_IN_OCTETS. */
    IF_HC_IN_OCTETS(6),
    /** IF_HC_IN_UCAST_PKTS. */
    IF_HC_IN_UCAST_PKTS(7),
    /** IF_HC_IN_MULTICAST_PKTS. */
    IF_HC_IN_MULTICAST_PKTS(8),
    /** IF_HC_IN_BROADCAST_PKTS. */
    IF_HC_IN_BROADCAST_PKTS(9),
    /** IF_HC_OUT_OCTETS. */
    IF_HC_OUT_OCTETS(10),
    /** IF_HC_OUT_UCAST_PKTS. */
    IF_HC_OUT_UCAST_PKTS(11),
    /** IF_HC_OUT_MULTICAST_PKTS. */
    IF_HC_OUT_MULTICAST_PKTS(12),
    /** IF_HC_OUT_BROADCAST_PKTS. */
    IF_HC_OUT_BROADCAST_PKTS(13),
    /** IF_LINK_UP_DOWN_TRAP_ENABLE. */
    IF_LINK_UP_DOWN_TRAP_ENABLE(14),
    /** IF_HIGH_SPEED. */
    IF_HIGH_SPEED(15),
    /** IF_PROMISCUOUS_MODE. */
    IF_PROMISCUOUS_MODE(16),
    /** IF_CONNECTOR_PRESENT. */
    IF_CONNECTOR_PRESENT(17),
    /** IF_ALIAS. */
    IF_ALIAS(18),
    /** IF_COUNTER_DISCONTINUITY_TIME. */
    IF_COUNTER_DISCONTINUITY_TIME(19);

    private static final OID IF_X_TABLE_OID = new OID(new int[] {1, 3, 6, 1, 2, 1, 31, 1, 1, 1});

    private final int columnIndex;

    private IfExTableColumn(final int columnIndex) {
        this.columnIndex = columnIndex;
    }

    @Override
    public OID get() {
        return new OID(IF_X_TABLE_OID.getValue(), columnIndex);
    }
}
