package hotspotinternet.tlr600.driver;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;

import hotspotinternet.tlr600.driver.impl.InterfaceReading;

/**
 * Driver interface for interacting with TP-Link Tlr600VPN router.
 */
public interface Tlr600Driver extends Closeable {
    /**
     * Get indices of interfaces that are configured as WAN interfaces on router.
     *
     * @return list of WAN interfaces
     */
    List<Integer> getWanInterfaces();

    /**
     * Opens for communications.
     *
     * @throws IOException if problem initiating comms
     */
    void open() throws IOException;

    /**
     * Gets interface readings.
     *
     * @return List of interface readings
     */
    List<InterfaceReading> getInterfaceReadings();

    /**
     * Enables or disables interface. The interface indices that may be provided as
     * an argument can be obtained from getInterfaceReadings.
     *
     * @param interfaceIndex of interface to enable or disable
     * @param isEnabled <code>true</code> to enable interface, <code>false</code> to disable
     * @throws IOException if problem enabling/disabling interface
     * @throws IllegalArgumentException if interfaceIndex does not match one of available indices
     */
    void setInterfaceEnabled(int interfaceIndex, boolean isEnabled) throws IOException;
}
